# application.py
#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import os, stat, subprocess, shlex, re, sys
from shutil import which


class BootInfo:
    def __init__(self):
        if "/sbin" not in os.environ["PATH"].split(':'):
            print(
                "'/sbin' is not included in PATH environment variable.\n this is most likely due to the lack of administrative privileges.")
            raise (TypeError)
        if not which("blkid"):
            print("blkid could not be located")
            raise (TypeError)
        if not which("lsblk"):
            print("blkid could not be located")
            raise (TypeError)
        out = subprocess.Popen(shlex.split("lsblk --json -o name,type,mountpoint,pttype,parttype,fstype,path"),
                               stdout=subprocess.PIPE).communicate()
        devices = json.loads(out[0])['blockdevices']
        self.devices = dict()
        for item in devices:
            if item["type"] == "disk":
                disk = dict()
                name = item["name"]
                disk["pttype"] = "Gpt" if item["pttype"] == "gpt" else "Mbr" if item["pttype"] == "dos" else "None"
                disk["boottype"] = self.get_mbr_boottype(item["path"])
                disk["parts"] = dict()
                if "children" in item:
                    for subitem in item["children"]:
                        if subitem["type"] == "part" :
                            part = dict()
                            partname = subitem["name"]
                            part["mountpoint"] = subitem["mountpoint"]
                            part["fstype"] = subitem["fstype"]
                            part["pttype"] = "Gpt" if subitem["pttype"] == "gpt" else "Mbr" if subitem["pttype"] == "dos" else "None"
                            part["parttype"] = self.get_gptparttype(subitem["parttype"]) if item["pttype"] == "gpt" else self.get_mbrparttype(subitem["parttype"][2:]) if item["pttype"] == "dos" else "None"
                            part["fstype"] = subitem["fstype"]
                            part["boottype"] = self.get_vbr_boottype(subitem["path"])
                            disk["parts"][partname] = part
                self.devices[name] = disk

    def get_mbrparttype(self,type):
        type_table = { # from https://www.win.tue.nl/~aeb/partitions/partition_types-1.html and https://en.wikipedia.org/wiki/Partition_type
            "00" :'Empty',
            "01" : 'FAT12',
            "02" : 'XENIX root',
            "03" : 'XENIX /usr',
            "04" : 'FAT16 <32M',
            "05" : 'Extended',
            "06" : 'FAT16',
            "07" : 'NTFS / exFAT / HPFS',
            "08" : 'AIX bootable',
            "09" : 'AIX data',
            "0A" : 'OS/2 Boot Manager',
            "0B" : 'W95 FAT32',
            "0C" : 'W95 FAT32 (LBA)',
            "0E" : 'W95 FAT16 (LBA)',
            "0F" : 'W95 Extended (LBA)',
            "10" : 'OPUS',
            "11" : 'Hidden FAT12',
            "12" : 'Compaq diagnostics',
            "14" : 'Hidden FAT16 < 32M',
            "16" : 'Hidden FAT16',
            "17" : 'Hidden NTFS / HPFS',
            "18" : 'AST SmartSleep',
            "1B" : 'Hidden W95 FAT32',
            "1C" : 'Hidden W95 FAT32 (LBA)',
            "1E" : 'Hidden W95 FAT16 (LBA)',
            "24" : 'NEC DOS',
            "27" : 'Hidden NTFS (Recovery Environment)',
            "2A" : 'AtheOS File System',
            "2B" : 'SyllableSecure',
            "32" : 'NOS',
            "35" : 'JFS on OS/2',
            "38" : 'THEOS',
            "39" : 'Plan 9',
            "3A" : 'THEOS',
            "3B" : 'THEOS Extended',
            "3C" : 'PartitionMagic recovery',
            "3D" : 'Hidden NetWare',
            "40" : 'Venix 80286',
            "41" : 'PPC PReP Boot',
            "42" : 'SFS',
            "44" : 'GoBack',
            "45" : 'Boot-US boot manager',
            "4D" : 'QNX4.x',
            "4E" : 'QNX4.x 2nd part',
            "4F" : 'QNX4.x 3rd part',
            "50" : 'OnTrack DM',
            "51" : 'OnTrack DM6 Aux1',
            "52" : 'CP/M',
            "53" : 'OnTrack DM6 Aux3',
            "54" : 'OnTrack DM6 DDO',
            "55" : 'EZ-Drive',
            "56" : 'Golden Bow',
            "57" : 'DrivePro',
            "5C" : 'Priam Edisk',
            "61" : 'SpeedStor',
            "63" : 'GNU HURD or SysV',
            "64" : 'Novell Netware 286',
            "65" : 'Novell Netware 386',
            "70" : 'DiskSecure Multi-Boot',
            "74" : 'Scramdisk',
            "75" : 'IBM PC/IX',
            "78" : 'XOSL filesystem',
            "80" : 'Old Minix',
            "81" : 'Minix / old Linux',
            "82" : 'Linux swap / Solaris',
            "83" : 'Linux',
            "84" : 'OS/2 hidden C: drive',
            "85" : 'Linux extended',
            "86" : 'NTFS volume set',
            "87" : 'NTFS volume set',
            "88" : 'Linux plaintext',
            "8A" : 'Linux Kernel (AiR-BOOT)',
            "8D" : 'Free FDISK hidden Primary FAT12',
            "8E" : 'Linux LVM',
            "90" : 'Free FDISK hidden Primary FAT16 <32M',
            "91" : 'Free FDISK hidden Extended',
            "92" : 'Free FDISK hidden Primary FAT16',
            "93" : 'Amoeba/Accidently Hidden Linux',
            "94" : 'Amoeba bad block table',
            "97" : 'Free FDISK hidden Primary FAT32',
            "98" : 'Free FDISK hidden Primary FAT32 (LBA)',
            "9A" : 'Free FDISK hidden Primary FAT16 (LBA)',
            "9B" : 'Free FDISK hidden Extended (LBA)',
            "9F" : 'BSD/OS',
            "A0" : 'IBM Thinkpad hibernation',
            "A1" : 'Laptop hibernation',
            "A5" : 'FreeBSD',
            "A6" : 'OpenBSD',
            "A7" : 'NeXTSTEP',
            "A8" : 'Darwin UFS',
            "A9" : 'NetBSD',
            "AB" : 'Darwin boot',
            "AF" : 'HFS / HFS+',
            "B0" : 'BootStar',
            "B1" : 'SpeedStor / QNX Neutrino Power-Safe',
            "B3" : 'SpeedStor / QNX Neutrino Power-Safe',
            "b2" : 'QNX Neutrino Power-Safe',
            "B4" : 'SpeedStor',
            "B6" : 'SpeedStor',
            "B7" : 'BSDI fs',
            "B8" : 'BSDI swap',
            "BB" : 'Boot Wizard hidden',
            "BC" : 'Acronis BackUp',
            "BE" : 'Solaris boot',
            "BF" : 'Solaris',
            "C0" : 'CTOS',
            "C1" : 'DRDOS / secured (FAT-12)',
            "C2" : 'Hidden Linux (PowerBoot)',
            "C3" : 'Hidden Linux Swap (PowerBoot)',
            "C4" : 'DRDOS secured FAT16 < 32M',
            "C5" : 'DRDOS secured Extended',
            "C6" : 'DRDOS secured FAT16',
            "C7" : 'Syrinx',
            "CB" : 'DR-DOS secured FAT32 (CHS)',
            "CC" : 'DR-DOS secured FAT32 (LBA)',
            "CD" : 'CTOS Memdump?',
            "CE" : 'DR-DOS FAT16X (LBA)',
            "CF" : 'DR-DOS secured EXT DOS (LBA)',
            "D0" : 'REAL/32 secure big partition',
            "DA" : 'Non-FS data / Powercopy Backup',
            "DB" : 'CP/M / CTOS / ...',
            "DD" : 'Dell Media Direct',
            "DE" : 'Dell Utility',
            "DF" : 'BootIt',
            "E1" : 'DOS access',
            "E3" : 'DOS R/O',
            "E4" : 'SpeedStor',
            "E8" : 'LUKS',
            "EB" : 'BeOS BFS',
            "EC" : 'SkyOS',
            "EE" : 'GPT',
            "EF" : 'EFI (FAT-12/16/32)',
            "F0" : 'Linux/PA-RISC boot',
            "F1" : 'SpeedStor',
            "F2" : 'DOS secondary',
            "F4" : 'SpeedStor',
            "FB" : 'VMware VMFS',
            "FC" : 'VMware VMswap',
            "FD" : 'Linux raid autodetect',
            "FE" : 'LANstep',
            "FF" : 'Xenix Bad Block Table'
        }
        if len(type) == 1:
            type = "0"+ type
        if type.upper() in type_table:
            return type_table[type.upper()]
        else:
            return  "Unknown"

    def get_gptparttype(self, guid):
        guid_table = { # from https://en.wikipedia.org/wiki/GUID_Partition_Table 9 may 2019
            "00000000-0000-0000-0000-000000000000": {"OS": "N/A", "Type": "Unused entry"},
            "024DEE41-33E7-11D3-9D69-0008C781F39F": {"OS": "N/A",
                                                     "Type": "MBR partition scheme"},
            "C12A7328-F81F-11D2-BA4B-00A0C93EC93B": {"OS": "N/A", "Type": "EFI System partition"},
            "21686148-6449-6E6F-744E-656564454649": {"OS": "N/A", "Type": "BIOS boot partition[e]"},
            "D3BFE2DE-3DAF-11DF-BA40-E3A556D89593": {"OS": "N/A",
                                                     "Type": "Intel Fast Flash (iFFS) partition (for Intel Rapid Start technology)"},
            "F4019732-066E-4E12-8273-346C5641494F": {"OS": "N/A", "Type": "Sony boot partition"},
            "BFBFAFE7-A34F-448A-9A5B-6213EB736C22": {"OS": "N/A", "Type": "Lenovo boot partition"},
            "E3C9E316-0B5C-4DB8-817D-F92DF00215AE": {"OS": "Windows", "Type": "Microsoft Reserved Partition (MSR)"},
            "EBD0A0A2-B9E5-4433-87C0-68B6B72699C7": {"OS": "Windows", "Type": "Basic data partition"},
            "5808C8AA-7E8F-42E0-85D2-E1E90434CFB3": {"OS": "Windows",
                                                     "Type": "Logical Disk Manager (LDM) metadata partition"},
            "AF9B60A0-1431-4F62-BC68-3311714A69AD": {"OS": "Windows", "Type": "Logical Disk Manager data partition"},
            "DE94BBA4-06D1-4D40-A16A-BFD50179D6AC": {"OS": "Windows", "Type": "Windows Recovery Environment"},
            "37AFFC90-EF7D-4E96-91C3-2D7AE055B174": {"OS": "Windows",
                                                     "Type": "IBM General Parallel File System (GPFS) partition"},
            "E75CAF8F-F680-4CEE-AFA3-B001E56EFC2D": {"OS": "Windows", "Type": "Storage Spaces partition"},
            "75894C1E-3AEB-11D3-B7C1-7B03A0000000": {"OS": "HP-UX", "Type": "Data partition"},
            "E2A1E728-32E3-11D6-A682-7B03A0000000": {"OS": "HP-UX", "Type": "Service Partition"},
            "0FC63DAF-8483-4772-8E79-3D69D8477DE4": {"OS": "Linux", "Type": "Linux filesystem data"},
            "A19D880F-05FC-4D3B-A006-743F0F84911E": {"OS": "Linux", "Type": "RAID partition"},
            "44479540-F297-41B2-9AF7-D131D5F0458A": {"OS": "Linux", "Type": "Root partition (x86)"},
            "4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709": {"OS": "Linux", "Type": "Root partition (x86-64)"},
            "69DAD710-2CE4-4E3C-B16C-21A1D49ABED3": {"OS": "Linux", "Type": "Root partition (32-bit ARM)"},
            "B921B045-1DF0-41C3-AF44-4C6F280D3FAE": {"OS": "Linux", "Type": "Root partition (64-bit ARM/AArch64)"},
            "0657FD6D-A4AB-43C4-84E5-0933C84B4F4F": {"OS": "Linux", "Type": "Swap partition"},
            "E6D6D379-F507-44C2-A23C-238F2A3DF928": {"OS": "Linux", "Type": "Logical Volume Manager (LVM) partition"},
            "933AC7E1-2EB4-4F13-B844-0E14E2AEF915": {"OS": "Linux", "Type": "/home partition"},
            "3B8F8425-20E0-4F3B-907F-1A25A76F98E8": {"OS": "Linux", "Type": "/srv (server data) partition"},
            "7FFEC5C9-2D00-49B7-8941-3EA10A5586B7": {"OS": "Linux", "Type": "Plain dm-crypt partition"},
            "CA7D7CCB-63ED-4C53-861C-1742536059CC": {"OS": "Linux", "Type": "LUKS partition"},
            "8DA63339-0007-60C0-C436-083AC8230908": {"OS": "Linux", "Type": "Reserved"},
            "83BD6B9D-7F41-11DC-BE0B-001560B84F0F": {"OS": "FreeBSD", "Type": "Boot partition"},
            "516E7CB4-6ECF-11D6-8FF8-00022D09712B": {"OS": "FreeBSD", "Type": "Data partition"},
            "516E7CB5-6ECF-11D6-8FF8-00022D09712B": {"OS": "FreeBSD", "Type": "Swap partition"},
            "516E7CB6-6ECF-11D6-8FF8-00022D09712B": {"OS": "FreeBSD", "Type": "Unix File System (UFS) partition"},
            "516E7CB8-6ECF-11D6-8FF8-00022D09712B": {"OS": "FreeBSD", "Type": "Vinum volume manager partition"},
            "516E7CBA-6ECF-11D6-8FF8-00022D09712B": {"OS": "FreeBSD", "Type": "ZFS partition"},
            "48465300-0000-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin",
                                                     "Type": "Hierarchical File System Plus (HFS+) partition"},
            "7C3457EF-0000-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple APFS"},
            "55465300-0000-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple UFS container"},
            "6A898CC3-1DD2-11B2-99A6-080020736631": {"OS": "MacOS / Darwin", "Type": "ZFS"},
            "52414944-0000-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple RAID partition"},
            "52414944-5F4F-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple RAID partition, offline"},
            "426F6F74-0000-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin",
                                                     "Type": "Apple Boot partition (Recovery HD)"},
            "4C616265-6C00-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple Label"},
            "5265636F-7665-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin", "Type": "Apple TV Recovery partition"},
            "53746F72-6167-11AA-AA11-00306543ECAC": {"OS": "MacOS / Darwin",
                                                     "Type": "Apple Core Storage (i.e. Lion FileVault) partition"},
            "B6FA30DA-92D2-4A9A-96F1-871EC6486200": {"OS": "MacOS / Darwin", "Type": "SoftRAID_Status"},
            "2E313465-19B9-463F-8126-8A7993773801": {"OS": "MacOS / Darwin", "Type": "SoftRAID_Scratch"},
            "FA709C7E-65B1-4593-BFD5-E71D61DE9B02": {"OS": "MacOS / Darwin", "Type": "SoftRAID_Volume"},
            "BBBA6DF5-F46F-4A89-8F59-8765B2727503": {"OS": "MacOS / Darwin", "Type": "SoftRAID_Cache"},
            "6A82CB45-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Boot partition"},
            "6A85CF4D-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Root partition"},
            "6A87C46F-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Swap partition"},
            "6A8B642B-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Backup partition"},
            "6A8EF2E9-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "/var partition"},
            "6A90BA39-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "/home partition"},
            "6A9283A5-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Alternate sector"},
            "6A945A3B-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Reserved partition"},
            "6A9630D1-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Reserved partition"},
            "6A980767-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Reserved partition"},
            "6A96237F-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Reserved partition"},
            "6A8D2AC7-1DD2-11B2-99A6-080020736631": {"OS": "Solaris / Illumos", "Type": "Reserved partition"},
            "49F48D32-B10E-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "Reserved partition"},
            "49F48D5A-B10E-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "FFS partition"},
            "49F48D82-B10E-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "LFS partition"},
            "49F48DAA-B10E-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "RAID partition"},
            "2DB519C4-B10F-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "Concatenated partition"},
            "2DB519EC-B10F-11DC-B99B-0019D1879648": {"OS": "NetBSD", "Type": "Encrypted partition"},
            "FE3A2A5D-4F32-41A7-B725-ACCC3285A309": {"OS": "Chrome OS", "Type": "Chrome OS kernel"},
            "3CB8E202-3B7E-47DD-8A3C-7FF2A13CFCEC": {"OS": "Chrome OS", "Type": "Chrome OS rootfs"},
            "2E0A753D-9E48-43B0-8337-B15192CB1B5E": {"OS": "Chrome OS", "Type": "Chrome OS future use"},
            "5DFBF5F4-2848-4BAC-AA5E-0D9A20B745A6": {"OS": "Container Linux by CoreOS",
                                                     "Type": "/usr partition (coreos-usr)"},
            "3884DD41-8582-4404-B9A8-E9B84F2DF50E": {"OS": "Container Linux by CoreOS",
                                                     "Type": "Resizable rootfs (coreos-resize)"},
            "C95DC21A-DF0E-4340-8D7B-26CBFA9A03E0": {"OS": "Container Linux by CoreOS",
                                                     "Type": "OEM customizations (coreos-reserved)"},
            "BE9067B9-EA49-4F15-B4F6-F36F8C9E1818": {"OS": "Container Linux by CoreOS",
                                                     "Type": "Root filesystem on RAID (coreos-root-raid)"},
            "42465331-3BA3-10F1-802A-4861696B7521": {"OS": "Haiku", "Type": "Haiku BFS"},
            "85D5E45E-237C-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "Boot partition"},
            "85D5E45A-237C-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "Data partition"},
            "85D5E45B-237C-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "Swap partition"},
            "0394EF8B-237E-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "Unix File System (UFS) partition"},
            "85D5E45C-237C-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "Vinum volume manager partition"},
            "85D5E45D-237C-11E1-B4B3-E89A8F7FC3A7": {"OS": "MidnightBSD", "Type": "ZFS partition"},
            "45B0969E-9B03-4F30-B4C6-B4B80CEFF106": {"OS": "Ceph", "Type": "Journal"},
            "45B0969E-9B03-4F30-B4C6-5EC00CEFF106": {"OS": "Ceph", "Type": "dm-crypt journal"},
            "4FBD7E29-9D25-41B8-AFD0-062C0CEFF05D": {"OS": "Ceph", "Type": "OSD"},
            "4FBD7E29-9D25-41B8-AFD0-5EC00CEFF05D": {"OS": "Ceph", "Type": "dm-crypt OSD"},
            "89C57F98-2FE5-4DC0-89C1-F3AD0CEFF2BE": {"OS": "Ceph", "Type": "Disk in creation"},
            "89C57F98-2FE5-4DC0-89C1-5EC00CEFF2BE": {"OS": "Ceph", "Type": "dm-crypt disk in creation"},
            "CAFECAFE-9B03-4F30-B4C6-B4B80CEFF106": {"OS": "Ceph", "Type": "Block"},
            "30CD0809-C2B2-499C-8879-2D6B78529876": {"OS": "Ceph", "Type": "Block DB"},
            "5CE17FCE-4087-4169-B7FF-056CC58473F9": {"OS": "Ceph", "Type": "Block write-ahead log"},
            "FB3AABF9-D25F-47CC-BF5E-721D1816496B": {"OS": "Ceph", "Type": "Lockbox for dm-crypt keys"},
            "4FBD7E29-8AE0-4982-BF9D-5A8D867AF560": {"OS": "Ceph", "Type": "Multipath OSD"},
            "45B0969E-8AE0-4982-BF9D-5A8D867AF560": {"OS": "Ceph", "Type": "Multipath journal"},
            "CAFECAFE-8AE0-4982-BF9D-5A8D867AF560": {"OS": "Ceph", "Type": "Multipath block"},
            "7F4A666A-16F3-47A2-8445-152EF4D03F6C": {"OS": "Ceph", "Type": "Multipath block"},
            "EC6D6385-E346-45DC-BE91-DA2A7C8B3261": {"OS": "Ceph", "Type": "Multipath block DB"},
            "01B41E1B-002A-453C-9F17-88793989FF8F": {"OS": "Ceph", "Type": "Multipath block write-ahead log"},
            "CAFECAFE-9B03-4F30-B4C6-5EC00CEFF106": {"OS": "Ceph", "Type": "dm-crypt block"},
            "93B0052D-02D9-4D8A-A43B-33A3EE4DFBC3": {"OS": "Ceph", "Type": "dm-crypt block DB"},
            "306E8683-4FE2-4330-B7C0-00A917C16966": {"OS": "Ceph", "Type": "dm-crypt block write-ahead log"},
            "45B0969E-9B03-4F30-B4C6-35865CEFF106": {"OS": "Ceph", "Type": "dm-crypt LUKS journal"},
            "CAFECAFE-9B03-4F30-B4C6-35865CEFF106": {"OS": "Ceph", "Type": "dm-crypt LUKS block"},
            "166418DA-C469-4022-ADF4-B30AFD37F176": {"OS": "Ceph", "Type": "dm-crypt LUKS block DB"},
            "86A32090-3647-40B9-BBBD-38D8C573AA86": {"OS": "Ceph", "Type": "dm-crypt LUKS block write-ahead log"},
            "4FBD7E29-9D25-41B8-AFD0-35865CEFF05D": {"OS": "Ceph", "Type": "dm-crypt LUKS OSD"},
            "824CC7A0-36A8-11E3-890A-952519AD3F61": {"OS": "OpenBSD", "Type": "Data partition"},
            "CEF5A9AD-73BC-4601-89F3-CDEEEEE321A1": {"OS": "QNX", "Type": "Power-safe (QNX6) file system"},
            "C91818F9-8025-47AF-89D2-F030D7000C2C": {"OS": "Plan 9", "Type": "Plan 9 partition"},
            "9D275380-40AD-11DB-BF97-000C2911D1B8": {"OS": "VMware ESX", "Type": "vmkcore (coredump partition)"},
            "AA31E02A-400F-11DB-9590-000C2911D1B8": {"OS": "VMware ESX", "Type": "VMFS filesystem partition"},
            "9198EFFC-31C0-11DB-8F78-000C2911D1B8": {"OS": "VMware ESX", "Type": "VMware Reserved"},
            "2568845D-2332-4675-BC39-8FA5A4748D15": {"OS": "Android-IA", "Type": "Bootloader"},
            "114EAFFE-1552-4022-B26E-9B053604CF84": {"OS": "Android-IA", "Type": "Bootloader2"},
            "49A4D17F-93A3-45C1-A0DE-F50B2EBE2599": {"OS": "Android-IA", "Type": "Boot"},
            "4177C722-9E92-4AAB-8644-43502BFD5506": {"OS": "Android-IA", "Type": "Recovery"},
            "EF32A33B-A409-486C-9141-9FFB711F6266": {"OS": "Android-IA", "Type": "Misc"},
            "20AC26BE-20B7-11E3-84C5-6CFDB94711E9": {"OS": "Android-IA", "Type": "Metadata"},
            "38F428E6-D326-425D-9140-6E0EA133647C": {"OS": "Android-IA", "Type": "System"},
            "A893EF21-E428-470A-9E55-0668FD91A2D9": {"OS": "Android-IA", "Type": "Cache"},
            "DC76DDA9-5AC1-491C-AF42-A82591580C0D": {"OS": "Android-IA", "Type": "Data"},
            "EBC597D0-2053-4B15-8B64-E0AAC75F4DB1": {"OS": "Android-IA", "Type": "Persistent"},
            "C5A0AEEC-13EA-11E5-A1B1-001E67CA0C3C": {"OS": "Android-IA", "Type": "Vendor"},
            "BD59408B-4514-490D-BF12-9878D963F378": {"OS": "Android-IA", "Type": "Config"},
            "8F68CC74-C5E5-48DA-BE91-A0C8C15E9C80": {"OS": "Android-IA", "Type": "Factory"},
            "9FDAA6EF-4B3F-40D2-BA8D-BFF16BFB887B": {"OS": "Android-IA", "Type": "Factory (alt)"},
            "767941D0-2085-11E3-AD3B-6CFDB94711E9": {"OS": "Android-IA", "Type": "Fastboot / Tertiary"},
            "AC6D7924-EB71-4DF8-B48D-E267B27148FF": {"OS": "Android-IA", "Type": "OEM"},
            "19A710A2-B3CA-11E4-B026-10604B889DCF": {"OS": "Android 6.0+ ARM", "Type": "Android Meta"},
            "193D1EA4-B3CA-11E4-B075-10604B889DCF": {"OS": "Android 6.0+ ARM", "Type": "Android EXT"},
            "7412F7D5-A156-4B13-81DC-867174929325": {"OS": "Open Network Install Environment", "Type": "Boot"},
            "D4E6E2CD-4469-46F3-B5CB-1BFF57AFC149": {"OS": "Open Network Install Environment", "Type": "Config"},
            "9E1A2D38-C612-4316-AA26-8B49521E5A8B": {"OS": "PowerPC", "Type": "PReP boot"},
            "BC13C2FF-59E6-4262-A352-B275FD6F7172": {"OS": "freedesktop.org OSes (Linux, etc.)",
                                                     "Type": "Shared boot loader configuration"},
            "734E5AFE-F61A-11E6-BC64-92361F002671": {"OS": "Atari TOS", "Type": "Basic data partition (GEM, BGM, F32)"}
        }
        if guid.upper() in guid_table:
            return guid_table[guid.upper()]["OS"], guid_table[guid.upper()]["Type"]
        else:
            return "Unknown", "Unknown"

    def get_parttabtype(self, part):
        out = subprocess.Popen(shlex.split("blkid '%s'" % part), stdout=subprocess.PIPE).communicate()
        m = re.search(r'PTTYPE="(\w+)"', str(out))
        if m:
            pttype = m.group(1)
            if pttype == "gpt":
                return "Gpt"
            elif pttype == "dos":
                return "Dos"
            else:
                return "Unknown"
        else:
            return None

    def get_vbr_boottype(self, part):
        with open(part, "rb") as f:
            f.seek(0x080)
            bsval = int.from_bytes(f.read(4), byteorder='big')
            bshex = "%04X" % (bsval >> 16)
            bswhole = "%08X" % bsval
            f.seek(0x1FE)
            bssign = int.from_bytes(f.read(2), byteorder='big')
            if bssign != 0X55AA:  # partition is not bootable
                return "Not Bootable"
            boottypelist = {
                "0069": 'ISOhybrid (Syslinux 3.72-3.73)',
                "010F": 'HP Recovery',
                "019D": 'BSD4.4: FAT32',
                "0211": 'Dell Utility: FAT16',
                "0488": "Grub2's core.img",
                "0689": 'Syslinux 3.00-3.52',
                "7405": 'Windows 7: FAT32',
                "0734": 'Dos_1.0',
                "0745": 'Windows Vista: FAT32',
                "089E": 'MSDOS5.0: FAT16',
                "08CD": 'Windows XP: NTFS',
                "0B60": 'Dell Utility: FAT16',
                "0BD0": 'MSWIN4.1: FAT32',
                "0E00": 'Dell Utility: FAT16',
                "0FB6": 'ISOhybrid with partition support (Syslinux 3.82-3.86)',
                "2A00": 'ReactOS',
                "2D5E": 'Dos 1.1',
                "31C0": 'Syslinux 4.03 or higher',
                "31D2": "Grub2's core.img",
                "3A5E": 'Recovery: FAT32',
                "407C": 'ISOhybrid (Syslinux 3.82-4.04)',
                "4216": 'Grub4Dos: NTFS',
                "4445": 'Dell Restore: FAT32',
                "55AA": {"55AA750A": 'Grub4Dos: FAT32', "*": 'Windows Vista/7: NTFS'},
                "55CD": 'FAT32',
                "5626": 'Grub4Dos: EXT2/3/4',
                "638B": 'Freedos: FAT32',
                "6616": 'FAT16',
                "696E": 'FAT16',
                "6974": 'BootIt: FAT16',
                "6F65": 'BootIt: FAT16',
                "6F6E": '- (Cleared BS)',
                "6F6F": '- (Cleared BS)',
                "6F74": 'FAT32',
                "7815": {"7815B106": 'Syslinux 3.53-3.86', "*": 'FAT32'},
                "7CC6": 'MSWIN4.1: FAT32',
                "7E1E": 'Grub4Dos: FAT12/16',
                "8A56": 'Acronis SZ: FAT32',
                "83E1": 'ISOhybrid with partition support (Syslinux 4.00-4.04)',
                "8EC0": 'Windows XP: NTFS',
                "8ED0": 'Dell Recovery: FAT32',
                "B106": 'Syslinux 4.00-4.02',
                "B600": 'Dell Utility: FAT16',
                "B6C6": 'ISOhybrid with partition support (Syslinux 3.81)',
                "B6D1": 'Windows XP: FAT32',
                "E2F7": 'FAT32, Non Bootable',
                "E879": 'ISOhybrid (Syslinux 3.74-3.80)',
                "E9D8": 'Windows Vista/7/8/10: NTFS',
                "F6F6": '- (cleared BS by FDISK)',
                "FA33": 'Windows XP: NTFS',
                "FBc0": 'ISOhybrid (Syslinux 3.81)',
                "48B4": 'Grub2 (v1.96)',
                "7C3C": 'Grub2 (v1.97-1.98)',
                "0020": 'Grub2 (v1.99-2.02)',
                "AA75": 'Grub Legacy',
                "5272": 'Grub Legacy',
                "8053": 'LILO',
                "0000": '- (No Boot)'
            }
            if bshex in boottypelist:
                name = boottypelist[bshex]
                if type(name) is str:
                    return name
                else:
                    for key, elem in name.items():
                        if key == bswhole:
                            return elem
                        elif key == '*':
                            return elem
            return "Unknown"

    def get_mbr_boottype(self, part):
        with open(part, "rb") as f:
            f.seek(0x000)
            bsval = f.read(8)
            bshex = []
            bshex.append(''.join(["%02X" % x for x in bsval[:2]]).strip())
            bshex.append(''.join(["%02X" % x for x in bsval[:3]]).strip())
            bshex.append(''.join(["%02X" % x for x in bsval]).strip())
            f.seek(0x080)
            bsval = int.from_bytes(f.read(2), byteorder='big')
            bshex.append("%04X" % bsval)
            f.seek(0x1FE)
            bssign = int.from_bytes(f.read(2), byteorder='big')
            if bssign != 0X55AA:  # partition is not bootable
                return "Not Bootable"
            boottypelist = {
                "EB48": "Grub Legacy",
                "EB4C": 'Grub2 (v1.96)',
                "EB63": {"*": {"*": {"7C3C": 'Grub2 (v1.97-1.98)', "0020": 'Grub2 (v1.99-2.02)'}}},
                "0EBE": 'ThinkPad',
                "31C0": {"*": {"31C08ED0BC007C8E": 'SUSE generic MBR', "31C08ED0BC007CFB": 'Acer PQService MBR'}},
                "33C0": {"33C08E": 'Windows', "33C090": 'DiskCryptor', "33C0FA": 'Syslinux MBR (4.04 and higher)'},
                "33ED": {"*": {"*": {"407C": 'ISOhybrid (Syslinux 4.04 and higher)',
                                     "83E1": 'ISOhybrid with partition support (Syslinux 4.04 and higher)',
                                     "*": "Unknown variant of SysLinux 4.04"}}},
                "33FF": 'HP/Gateway',
                "B800": 'Plop',
                "EA05": 'XOSL',
                "EA1E": 'Truecrypt Boot Loader',
                "EB04": 'Solaris',
                "EB31": 'Paragon',
                "EB5E": {"EB5E00": 'fbinst', "EB5E80": 'Grub4Dos', "EB5E90": 'WEE'},
                "FA31": {"FA31C0": {"*": {"0069": 'ISOhybrid (Syslinux 3.72-3.73)', "7C66": 'Syslinux MBR (3.61-4.03)',
                                          "7CB8": 'Syslinux MBR (3.36-3.51)', "B442": 'Syslinux MBR (3.00-3.35)',
                                          "BB00": 'Syslinux MBR (3.52-3.60)', "E879": 'ISOhybrid (Syslinux 3.74-3.80)',
                                          "*": "Unknown type of Syslinux"}},
                         "FA31C9": 'Master Boot LoaDeR',
                         "FA31ED": {"*": {"0069": 'ISOhybrid (Syslinux 3.72-3.73)',
                                          "0FB6": 'ISOhybrid with partition support (Syslinux 3.82-3.86)',
                                          "407C": 'ISOhybrid (Syslinux 3.82-4.03)',
                                          "83E1": 'ISOhybrid with partition support (Syslinux 4.00-4.03)',
                                          "B6C6": 'ISOhybrid with partition support (Syslinux 3.81)',
                                          "FBC0": 'ISOhybrid (Syslinux 3.81)', "*": "Unknown type of Syslinux"}}},
                "FA33": 'MS-DOS 3.30 through Windows 95 (A)',
                "FAB8": {"*": {"FAB80000": 'FreeDOS (eXtended FDisk)', "*": "No boot loader"}},
                "FABE": 'No boot loader?',
                "FAEB": 'Lilo',
                "FAFC": 'ReactOS',
                "FC31": 'Testdisk',
                "FC33": 'GAG',
                "FCEB": 'BootIt NG',
                "0000": 'No boot loader',
            }
            curdict = boottypelist
            for hexval in bshex:
                newdict = None
                for key, elem in curdict.items():
                    if key == hexval or key == "*":
                        newdict = elem
                        if type(newdict) is str:
                            return newdict
                        else:
                            break
                if newdict is not None:
                    curdict = newdict
                else:
                    break
            return "Unknown"
