# application.py
#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .escape import Escape
from .bootinfo import BootInfo

class application():
    def __init__(self):
        print()

    def help_std(self, cmd):
        print("Help :")
        for group, text in self.commands[self.cmd_type]["groups"].items():
            print()
            print(Escape(text).bright_white().bold())
            for command, list_cmd in self.commands[self.cmd_type]["commands"].items():
                if list_cmd[1] == group:
                    print(command + " " + list_cmd[2])
        print()

        print(Escape(""))
        return 0, 0

    def quit_std(self, cmd):
        print("Quit application without saving")
        return 1, 0

    def invalid_cmd(self, cmd):
        print(Escape(cmd + " : unknown command").red())
        return 0, 0

    commands = {
        "std": {
            "prompt": "Command (h for help) :",
            "groups": {
                "other": "Other commands",
                "save_quit": "Save and Quit"
            },
            "commands": {
                "h": (help_std, "other", "Display Help"),
                "q": (quit_std, "save_quit", "Quit without saving changes"),
                "inv": (invalid_cmd, "hidden", "invalid command"),
            },
            "input_cmd": {
                "h": "h",
                "help": "h",
                "q": "q",
                "quit": "q"
            }
        }
    }

    def run(self):
        try:
            bootinfo = BootInfo()
        except TypeError:
            return
        type = bootinfo.get_mbr_boottype("/dev/sdb")
        print(type)
