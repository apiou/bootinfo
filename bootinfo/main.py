# main.py
#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from .arg_parser import arg_parser
from .application import application

def main(version):
    Params = sys.argv
    parser = arg_parser()
    args = parser.parse_args(Params)

    if args.version == True:
        print("bootinfo version "+ version)
        return 0
    app = application()
    app.args = args
    ret = app.run()
    return ret
