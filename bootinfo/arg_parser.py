#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

class arg_parser():
    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Get Boot information from your system")
        self.parser.add_argument('-v', '--version', help='Print version and exit', action='store_true')
        self.parser.add_argument('device', help='Device on which operation is performed', nargs='?')

    def parse_args(self,args):
        res = self.parser.parse_args(args[1:])
        return res

    def print_help(self):
        self.parser.print_help()
